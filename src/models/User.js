const bcrypt = require('bcryptjs');
const uuidv1 = require('uuid/v1');

class User {
    constructor(username, password) {
        // Talvez a variavel "cloud_provider" seja removida
        this.id = uuidv1();
        this.username = username;

        this.password = password;
        this.active = true;
    }

    getAttrs() {
        return {
            id: this.id,
            username: this.username,
            active: this.active,
            password: this.password,

        }
    }

    async verifyPassword(password) {
        if (JSON.stringify(password) === JSON.stringify(this.password)) {
            return 1;
        }
    }

    static async cryptPassword(password) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        return hash
    }

}

module.exports = User;
