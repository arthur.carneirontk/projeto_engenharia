const AWS = require('aws-sdk');
const badrequest = {
    statusCode: 400,
    headers: {
        'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
        created: false,
        message: "Bad Request"
    })
};


module.exports.handle = async event => {


    let { body } = event;
    body = JSON.parse(body);
    if (!body) return badrequest;
    let { username } = body;
    if (!(username)) return {
        statusCode: 400,
        message: "invalid user"
    };


    const docClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });


    let params = {
        TableName: process.env.DYNAMODB_TABLE,
        Key: {
            username
        },
    };

    let data = {};

    try {
        let data = await docClient.get(params).promise();
        if (Object.keys(data).length === 0) return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                success: false,
                message: "Usuario não foi encontrado",
            })
        };
    } catch (error) {
        return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                success: false,
                message: "Não foi possivel concluir a solicitação",
                error,
            })
        };

    }

    
    

    let updateParamns = {
        TableName: process.env.DYNAMODB_TABLE,
        Key: {
            username
        },
        UpdateExpression: "set password = :password",
        ExpressionAttributeValues: {
            ":password": '123',
        },
        ReturnValues: "UPDATED_NEW"
    }

    try {
        await docClient.update(updateParamns).promise();
    } catch (err) {
        return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                message: "Error",
                updated: false,
                err,
            })
        };
    }

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
            message: "Senha atualizada para a senha padrão",
            success: true,
        })
    };

};



