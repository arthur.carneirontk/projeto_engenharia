'use strict';

const AWS = require('aws-sdk');
const User = require('../models/User');

const badrequest = {
    statusCode: 400,
    headers: {
        'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
        created: false,
        message: "Bad Request"
    })
};


module.exports.handle = async event => {

    let { body } = event;
    console.log("Body> ", body)
    body = JSON.parse(body);
    if (!body) return badrequest;
    let { username, password } = body;
    if (!(username && password)) return badrequest;

    console.log("Chegou aqui")
    const docClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });

    let params = {
        TableName: process.env.DYNAMODB_TABLE,
        Key: {
            username
        },
    };

    let data = {};

    try {
        console.log("entrou no try");
        let data = await docClient.get(params).promise();
        if (data.Item) return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                created: false,
                message: "User already exists",
            }),
        };
    } catch (error) {
        return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                created: false,
                message: "Error",
                error
            })
        };
    }



    password = await User.cryptPassword(password);
    const user = new User(username, password);

    params = {
        TableName: process.env.DYNAMODB_TABLE,
        Item: user.getAttrs(),
    };

    data = {};
    try {
        data = await docClient.put(params).promise();
    } catch (error) {
        return {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
            body: JSON.stringify({
                created: false,
                message: "User dont created",
                error
            })
        };
    }

    return {
        statusCode: 201,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
            created: true,
            message: "User created",
        })
    }
};
