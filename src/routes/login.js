const AWS = require('aws-sdk');
const User = require("../models/User");


const badrequest = {
  statusCode: 400,
  headers: {
    'Access-Control-Allow-Origin': '*',
  },
  body: JSON.stringify({
    auth: false,
    message: "Bad Request"
  })
}

module.exports.handle = async event => {

  let { body } = event;
  body = JSON.parse(body);
  if (!body) return badrequest
  let { username, password } = body;
  if (!(username && password)) return badrequest;
  console.log("passou")

  const docClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1" });

  const paramns = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      username,
    },
  };



  let data = {}
  try {
    data = await docClient.get(paramns).promise();
  } catch (err) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      body: JSON.stringify({
        auth: false,
        message: "Error",
        err,
      })
    };
  }

  if (!data.Item) return {
    statusCode: 400,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
      auth: false,
      message: "User dont found",
    })
  }



  const password_ = data.Item.password;
  const user = new User(username, password_);

  console.log("Pass.", password);

  const validPassword = await user.verifyPassword(password);

  if (!(validPassword)) return {
    statusCode: 400,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
      auth: false,
      message: "Invalid password"
    })
  }

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({
      auth: true,
      message: "Login successful",
    }),
  };
};