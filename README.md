## Gerenciamento e Autenticação de Senhas

### Descrição

Este repositorio tem contido os arquivos do código-fonte e informações sobre o assunto abordado no projeto.

---

### Copyright (c)

Arthur Carneiro da Rocha Menezes

---

### O que é a segurança da informação?

Segurança da Informação envolve um conjunto de medidas necessárias por garantir que a confidencialidade, integridade e disponibilidade das informações de uma organização ou indivíduo de forma a preservar esta informação de acordo com necessidades específicas.

#### E qual  o objetivo da Segurança da Informação?

- Confidencialidade: um princípio de segurança que requer que dados devam somente ser acessados por pessoas autorizadas.

- Integridade: um princípio de segurança que garante que dados e itens de configuração somente sejam modificados por pessoas e atividades autorizadas. A integridade considera todas as possíveis causas de modificação, incluindo falhas de hardware e software, eventos ambientais e intervenção humana.

- Disponibilidade: o princípio de segurança que que garante que a informação que requer que dados devam ser acessados por pessoas autorizadas, no momento que requisitados.
---

### Boas Práticas

Agora que entendemos o que é e o que podemos esperar da **tecnologia da informação**, vamos adentrar em boas práticas para que possamos estar o mais seguros que der.

#### Primeira Dica - Atualização
 - Sempre mantenha os softwares que você e sua empresa usa(m) atualizados.
 Mantenha os sistemas operacionais originais e atualizados, os fabricantes estão sempre em busca de correções e atualizações contra ataques cibernéticos.
As atualizações de software são  importantes porque a maioria dos malwares por aí não tem exatamente como alvo vulnerabilidades de segurança novas e desconhecidas.

#### Segunda Dica - Gestão de Incidentes

- É sempre bom que seja adotado alguma ferramenta para gestão dos incidentes, uma governança administrativa que priorize a proteção dos dados e arquivos é essencial. Infelizmente, muitas empresas colocam muito foco na conformidade, pensando que, desde que cumpram todos os regulamentos, seus dados confidenciais serão totalmente protegidos.

#### Terceira Dica - Controle o que é seu!

- __Tente ter um controle sobre os acessos__. Comece por registrar todos os níveis e controles de acesso atuais em vigor. Após, verifique as funções das pessoas na empresa para definir o acesso a uma funcionalidade específica. Deve ser assegurado o acesso de usuário autorizado aos locais estritamente necessários ao desenvolvimento de suas tarefas.

#### Quarta Dica - No pior caso, tenha um backup.

- O **backup** ou cópia de segurança é um mecanismo fundamental para assegurar a disponibilidade da informação, caso as bases de dados em que a informação esteja armazenada sejam roubadas ou danificadas. Uma das etapas mais importantes do backup é descobrir com que frequência os dados precisam ser armazenados em backup. Idealmente, as empresas devem fazer o backup dos dados com a frequência que seus recursos permitirem.

#### Quinta Dica - Regras são regras.

- Todos os colaboradores fazem parte do processo de segurança da informação. Afinal, em alguma medida eles interferem no acesso às informações, seja por meio da criação de documentos, acesso à dados, facilitando a entrada de malwares com usos inadequados, etc.

- Por isso é fundamental estabelecer normas de conduta e políticas de segurança que devem ser seguidos por todos. Esse tipo de documentação permite normatizar as regras utilizadas na empresa.

#### Sexta Dica - Treinamento

- Algumas questões elaboradas nas políticas de segurança podem não ser tão claras para os colaboradores, principalmente por envolverem questões específicas da área de tecnologia.

- Para evitar confusões, dúvidas e ações errôneas, é imprescindível realizar treinamento com todos os envolvidos, a fim de normatizar as condutas de todos, bem como ensinar medidas básicas de segurança.

#### Setima Dica - Criptografia



- Neste software, por exemplo, é utilizada a criptografia de HASH MD5, uma que é considerada impossível de se crackear sem a senha.

- A **criptografia é uma importante aliada para a segurança da informação**. Ela impede, por exemplo, que os arquivos sejam acessados caso sejam interceptados no meio do processo, só tendo as chaves de acesso, as pessoas que possuem a chave privada.

![Criptografia](https://upload.wikimedia.org/wikipedia/commons/f/f8/Crypto.png "Criptografia")

---

### Mercado da Segurança da Informação
- O investimento em segurança da informação deve crescer nos próximos anos. Como tendência há o destaque da necessidade de medidas continuadas de prevenção, acompanhando a evolução dos riscos e formas de ataques.

- Vale a pena pesquisar sobre empresas que forneçam soluções para proteção de alto nível tecnológico de mesmo patamar ao de empresas globais, mas com tecnologia nacional e um preço justo e mais alinhado ao cenário e orçamento das empresas brasileiras.



