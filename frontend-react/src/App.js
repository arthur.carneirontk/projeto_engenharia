import React, { useState } from 'react';
import axios from 'axios';
import './App.css';

function App() {

  const [user, setUser] = useState({});
  function handlerChanger(e) {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
  }

  function handlerLogin(e) {
    console.log({ username: user.usuario, password: user.senha });
    axios.post('https://octuh9sdc9.execute-api.us-east-1.amazonaws.com/dev/login', { username: user.usuario, password: user.senha })
      .then(() => {
        
        alert("Logado com sucesso!");
        window.location = 'https://gitlab.com/arthur.carneirontk/projeto_engenharia/blob/master/README.md';
      })
      .catch(() => {
        alert("Error! Verifique os Logs!");
        console.log(user);
      })
  }

  function handlerReset(e) {
    console.log("User:", user)

    axios.post('https://octuh9sdc9.execute-api.us-east-1.amazonaws.com/dev/reset', { username: user.usuario })
      .then(() => {
        alert("Resetado com sucesso!");
      })
      .catch(() => {
        alert("Error! Verifique os Logs!");
      })

  }

  function handlerCreate(e) {
    console.log("User:", user)

    axios.post('https://octuh9sdc9.execute-api.us-east-1.amazonaws.com/dev/create', { username: user.usuario, password: user.senha })
      .then((response) => {
        alert("Criado com sucesso!");


      })
      .catch(() => {
        alert("Error! Verifique os Logs!");
      })

  }

  return (
    <div id="app">
      <div id="login">
        <div id="description">
          <h1>Gerenciamento e Atutenticação de Senhas</h1>
          <p>Projeto de Engenharia de Requisitos. Copyright (c) Arthur Carneiro da Rocha Menezes</p>
        </div>
        <div id="form">
          <label htmlFor="email">Usuário</label>
          <input type="text" id="email" name="usuario" onChange={handlerChanger} placeholder="Nome de usuário" autoComplete="off" />
          <label htmlFor="password">Senha</label>
          <input type="password" id="password" name="senha" onChange={handlerChanger} placeholder="*******" />
          <button type="submit" onClick={handlerLogin}   >Login</button>
          <button type="submit" onClick={handlerReset}>Esqueci a senha</button>
          <button type="submit" onClick={handlerCreate} >Cadastrar-me</button>
        </div>
      </div>
    </div>
  )
}
export default App;
